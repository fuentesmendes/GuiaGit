# GUIA GIT GITHUB
### ===========================================================================
## NUEVO REPOSITORIO

### 1.- git init   ==> Si el repositorio no halla sido clonado se procede de esta forma, sino NO.
### 2.- git add .   
### 3.- git add nombrearchivo    ==> Se agrega los archivos al repositorio local GIT
### 4.- git commit -m 'Mensaje del commit' 
### 5.- git remote add origin [URL DEL REPOSITORIO]
### 6.- git push -u origin master  ==> Se sube al repositorio remoto los cambios.

### ===========================================================================

##  TRAER CAMBIOS DEL REPOSITORIO REMOTO (Actualizar el repositorio local)

### 1.- git pull

### ===========================================================================

## RAMAS

### 1.- git checkout -b nombredelarama   ==> Crear nueva rama
### 2.- git checkout nombredelarama  ==> cambiar a otra rama 'nombredelarama'
### 3.- git branch    ==> Listado de las ramas
### 4.- git branch -d nombre_rama  ==> Error si contiene cambios recientes
### 5.- git branch -D nombre-rama  ==> Forzar el borrado
### 6.- git push origin :nombre-rama  ==> Borrar rama del repositorio remoto

### ===========================================================================


